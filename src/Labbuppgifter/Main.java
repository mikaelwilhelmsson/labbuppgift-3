package Labbuppgifter;

import java.util.List;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("[aeiouy]{2,}");


        List.of("youtube", "Mikael", "kiwi", "frukt", "skal")
                .stream()
                .filter(word -> pattern.matcher(word).find())
                .forEach(System.out::println);

    }
}
